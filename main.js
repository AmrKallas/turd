let containerLetter=document.querySelector(".letters")
let containerWord=document.querySelector(".word");
let draw=document.querySelector(".draw")

//option
let letter="abcdefghijklmnopqrstuvwxyz";
let arrayLetter=Array.from(letter);
let WrongAttempt=0;
let RightAttempt=0;
let status=false;

arrayLetter.forEach(letter=>{
    let span=document.createElement("span");

    span.className="let"
    
    let text=document.createTextNode(letter);

    span.appendChild(text);

    containerLetter.appendChild(span)
})

let words={
    programming:['Php','Cpp','Javascript','Python','Html','Css','Jquery'],
    people:['Amr','Majed','Abd alrahman','Islam','Ahmed','Mohammed','Bshar','Ibrahim','Nour'],
    contry:['Syria','Egypt','Palastine','Qatar','Lebanon','Barazil']    
};

let key=Object.keys(words);

let randomKey=Math.floor(Math.random() * key.length);

let theChosenArray=key[randomKey];

document.querySelector(".header .category").innerHTML=theChosenArray;

let randomNumberWord=Math.floor(Math.random() * words[theChosenArray].length);

let theChosenWord=words[theChosenArray][randomNumberWord];

let ArrayWord=Array.from(theChosenWord.toLowerCase());

ArrayWord.forEach(word=>{
    let span=document.createElement("span");

    containerWord.appendChild(span);

    if(word===' '){
        span.className="space";
    }
    else{
        span.className="letter";
    }
})

let Allspan=document.querySelectorAll(".word span");

containerLetter.addEventListener("click",(e) => {

    status=false;

    if(e.target.classList.contains("let")){

        e.target.classList.add("clicked");

    ArrayWord.forEach((word,indexWord)=>{

        if(e.target.innerHTML==word){

            status=true;

            Allspan.forEach((span,indexSpan)=>{

                if(indexWord===indexSpan){

                    span.innerHTML=word;

                    RightAttempt++;

                    document.querySelector(".sound .success").play();

                }

            })
        }
    })
    if(status==false){
        document.querySelector(".sound .fail").play();
        WrongAttempt++;
        draw.classList.add(`wrong_${WrongAttempt}`);
    }
    if(WrongAttempt==9){
        let lost=document.createElement("div");

        lost.className="loser";

        let textLost=document.createTextNode(`You Are Loser And Your Fail Attempt Is ${WrongAttempt}`);

        lost.appendChild(textLost);

        document.body.appendChild(lost);

        containerLetter.style.pointerEvents="none";

    }
    if(RightAttempt===Allspan.length){
        let win=document.createElement("div");

        win.className="winner";

        let textLost=document.createTextNode(`You Are winner And Your Fail Attempt Is ${WrongAttempt}`);

        win.appendChild(textLost);

        document.body.appendChild(win);

        containerLetter.style.pointerEvents="none";

    }
}
})
